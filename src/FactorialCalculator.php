<?php

namespace Packages\Practice;

class FactorialCalculator {
    public static function calculate($number)
    {
        if ($number < 0) {
            return 'The number must not be negative';
        }

        if ($number === 0) {
            return 1;
        }

        $result = 1;
        for ($i = 1; $i <= $number; $i++) {
            $result *= $i;
        }

        return $result;
    }
}
